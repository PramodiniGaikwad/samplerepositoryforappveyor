rem csc -help

@echo off
cls
echo "------------------------------------------------------------------------------------------------------------------"
echo %APPVEYOR_REPO_BRANCH%
echo "------------------------------------------------------------------------------------------------------------------"

rem for run msbuild sonare runner from bitbucket package
"C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" begin /k:"AzureVMAnalysis" /n:"Eisk MVC" /v:"1.0"

rem cd %pathMSBuild%
"C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe" "C:\projects\feature\samplerepositoryforappveyor\Eisk.MVC-VS2012.sln" /p:configuration=debug

rem to create package in zip
"C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe" "C:\projects\feature\samplerepositoryforappveyor\Eisk.MVC\Bin" /t:Package /p:PackageLocation=EISK.zip

"C:\projects\samplerepositoryforappveyor\MSBuild.SonarQube.Runner-2.0\MSBuild.SonarQube.Runner.exe" end

rem set nugetpath="C:\projects\samplerepositoryforappveyor\Eisk.MVC\"
rem cd %nugetpath%
rem appveyor PushArtifact Eisk.MVC.5.6.100.0.nupkg

rem set zippath="C:\projects\samplerepositoryforappveyor\Eisk.MVC\bin"
rem cd %zippath%
rem appveyor PushArtifact Eisk.MVC.zip